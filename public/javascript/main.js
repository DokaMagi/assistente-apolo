let recognitionSActivated = false

$(document).ready(() => {

    var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
    var recognition = new SpeechRecognition()
    recognition.lang = "pt-BR"

    recognition.onresult = function(e) {
        var transcript = e.results[0][0].transcript
        console.log(transcript)
        recognitionSActivated = false
        $(".talk-button").removeClass("transition-button-pressed")
        $(".layer").one('animationiteration webkitAnimationIteration', function() {
            $(this).removeClass("circle-animation");
        });
    }

    $(".talk-button").click((e) => {
        $(".layer").addClass("circle-animation")
        $("svg").addClass('circle-animation')

        recognitionSActivated = true
        e.target.classList.add("transition-button-pressed")
        recognition.start()
        

        
    });

})
